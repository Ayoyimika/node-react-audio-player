var path = require('path');

module.exports = {
  testMatch: ['**/*.test.+(js|ts|tsx)'],
  rootDir: path.join(process.cwd(), './'),
  moduleFileExtensions : ["ts","tsx","js","jsx","json"],
  transform : {
       "^.+\\.(ts|tsx|js|jsx)$": "/usr/share/nodejs/babel-jest/"
    },
};

